const moment = require('moment'); // Assurez-vous d'installer moment via npm

module.exports = {
    types: {
        date: (value, context) => {
            return moment(value).format('DD-MM-YYYY');
        }
    }
};