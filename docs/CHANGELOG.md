# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.11.0](https://gitlab.com/mauriau/autochangelog/compare/v1.10.0...v1.11.0) (2024-10-22)

* Merge branch 'batch/prod-command' into 'main' ([](https://gitlab.com/mauriau/autochangelog/commit/bc64e4e26481b0bb9a00104a0cc1fc7d463989b8))

### feat

* fake 1 ([](https://gitlab.com/mauriau/autochangelog/commit/2e845d7da21879dd41309c9235e39352ae5ca87c))
* fake 2 ([](https://gitlab.com/mauriau/autochangelog/commit/4e11b1407c97b7a46953525144e97c45bf63ed51))
* try with french date and new commit merge ([](https://gitlab.com/mauriau/autochangelog/commit/1f67cf56b1a74a338b500c10c2bc38115e8198e0))

## [1.10.0](https://gitlab.com/mauriau/autochangelog/compare/v1.9.0...v1.10.0) (2024-10-21)

* Merge branch 'batch/prod-command' into 'main' ([](https://gitlab.com/mauriau/autochangelog/commit/e5cc77e0ff9f2306ddcc29070109f4c5d936b58a))

### feat

* use conventionalcommits preset ([](https://gitlab.com/mauriau/autochangelog/commit/4f90470e39fcc66bbf47436bf7e2faf588f2478f))

# [1.9.0](https://gitlab.com/mauriau/autochangelog/compare/v1.8.0...v1.9.0) (2024-10-21)


* Merge branch 'batch/prod-command' into 'main' ([](https://gitlab.com/mauriau/autochangelog/commit/5c0f8e956a45c4ff0000b2eb79b3739b2e5ea227))
* Merge branch 'batch/prod-command' into 'main' ([](https://gitlab.com/mauriau/autochangelog/commit/871ab47b20bada9c608228cc5ac2bab63e4c85d3))
* Merge branch 'batch/prod-command' into 'main' ([](https://gitlab.com/mauriau/autochangelog/commit/8f2fd2ac645e41df044314275558b672ea063dc2))


### feat

* add cache on gitlab ([](https://gitlab.com/mauriau/autochangelog/commit/3b537f2e2d985ec51f1d4f8442438b9c2bc14b7b))
* fix commit migration ([](https://gitlab.com/mauriau/autochangelog/commit/fe6d5b0f489242d38cc0b73bb729bf72dd6fd0a5))
* **migration:** detect migration message in merge commit ([](https://gitlab.com/mauriau/autochangelog/commit/667cf63e88f19265acf33667744d3b75290f6c99))
* **migration:** detect migration message in merge commit - 2 ([](https://gitlab.com/mauriau/autochangelog/commit/fb00394627433a6863208031878384db13f93798))

## [1.8.0](https://gitlab.com/mauriau/autochangelog/compare/v1.7.0...v1.8.0) (2024-10-21)

### Features

* restore releaserc config ([58b3efb](https://gitlab.com/mauriau/autochangelog/commit/58b3efb172633c49401f4448bcd70e0be9c1ae70))
* test 3 post migration ([1274fcc](https://gitlab.com/mauriau/autochangelog/commit/1274fcc0e6fadc9892db4d98baa40094b47b65a5))
* test run something ([62b32df](https://gitlab.com/mauriau/autochangelog/commit/62b32dfdef88e6838d5beb0bb676f5ee106d4393))

## [1.7.0](https://gitlab.com/mauriau/autochangelog/compare/v1.6.0...v1.7.0) (2024-10-21)

### Features

* add commande for migration ([bee5ee3](https://gitlab.com/mauriau/autochangelog/commit/bee5ee3b45c0ab4cca291f03391078f28196e17e))

## [1.6.0](https://gitlab.com/mauriau/autochangelog/compare/v1.5.0...v1.6.0) (2024-10-21)

### Features

* add commande for migration ([6dd17aa](https://gitlab.com/mauriau/autochangelog/commit/6dd17aa3c641ea8212f8ce81a5f4169f0f8c90b9))

## [1.5.0](https://gitlab.com/mauriau/autochangelog/compare/v1.4.0...v1.5.0) (2024-10-21)

### Features

* i forget the feature name ([6099525](https://gitlab.com/mauriau/autochangelog/commit/60995256e2f7f32e5265545007698a2d1e4f4792))
* **test-4:** add field test on answer entity ([7515082](https://gitlab.com/mauriau/autochangelog/commit/7515082012edff150be8617c747e91627f57d76b))
* **test-4:** add field test on answer form ([209d324](https://gitlab.com/mauriau/autochangelog/commit/209d32439ce1e6fed7f7ee24ea8b6848641be7cb))
* **test-4:** add field test on api for answer ([7c65834](https://gitlab.com/mauriau/autochangelog/commit/7c65834a778634b26708a6bf446697bf6f02ca06))

### Miscellaneous Chores

* **test-4:** fix typo on answer ([4c31e1a](https://gitlab.com/mauriau/autochangelog/commit/4c31e1a1d38c1a1818bab38a4a28e22ec0193f1b))

## [1.4.0](https://gitlab.com/mauriau/autochangelog/compare/v1.3.0...v1.4.0) (2024-10-21)

### Features

* empty 1211 ([de19fff](https://gitlab.com/mauriau/autochangelog/commit/de19fff154973f667bd6c5798e32bebfe9985c6d))
* empty 12112 ([1671d7f](https://gitlab.com/mauriau/autochangelog/commit/1671d7f378919a994aea58e876d077597f16baac))
* test 2 ([b4f4b10](https://gitlab.com/mauriau/autochangelog/commit/b4f4b10d97695d50e5c25ccadb6455bbb1d9703b))

### Bug Fixes

* emtpy 12113 ([8685a61](https://gitlab.com/mauriau/autochangelog/commit/8685a61d6aff077ebeaca8e6845a08d8f23f2c8a))

## [1.3.0](https://gitlab.com/mauriau/autochangelog/compare/v1.2.0...v1.3.0) (2024-10-21)

### Features

* **commit-merge:** only commit of merge is in changelog ([7a0683b](https://gitlab.com/mauriau/autochangelog/commit/7a0683b5ccf8f45cff8a2ef7b1aa3c24a6accc68))
* empty 1 ([5b939c3](https://gitlab.com/mauriau/autochangelog/commit/5b939c372a971c4b23c92b935a6cee105c79f31e))
* emtpy 2 ([8c427e1](https://gitlab.com/mauriau/autochangelog/commit/8c427e14b01c6f2d02ee48d1470c9b30f4560719))
* only merge commit 2 ([d163ffd](https://gitlab.com/mauriau/autochangelog/commit/d163ffd45a01c5d126217de8e09d532ae9ca4ebd))
* **only-merge:** add conf ([fda2a50](https://gitlab.com/mauriau/autochangelog/commit/fda2a501fe826fbd2ecc11ddf1beb84b4bf8e1ed))
* **only-merge:** udpate conf ([923cc9f](https://gitlab.com/mauriau/autochangelog/commit/923cc9fb25b6260726dfdcfb51715532584ffeb0))

### Bug Fixes

* empty 2 ([db95c4b](https://gitlab.com/mauriau/autochangelog/commit/db95c4b047f7d9a79104eecf6235d17dd86fa2d8))
* emtpy 3 ([821d872](https://gitlab.com/mauriau/autochangelog/commit/821d872271a8733c41d7be44298ecb5439d98fab))
* file ([9f5f49e](https://gitlab.com/mauriau/autochangelog/commit/9f5f49e64d5bb110eb6c7a54c0fb27de2c30f4a3))

### Miscellaneous Chores

* empty 1 ([5c1af5c](https://gitlab.com/mauriau/autochangelog/commit/5c1af5c0d67db00203de9dedbb07d70aa2ffcaca))
* empty 5 ([107c867](https://gitlab.com/mauriau/autochangelog/commit/107c867784916ead06c9b72339a4434fef7bb5da))

## [1.2.0](https://gitlab.com/mauriau/autochangelog/compare/v1.1.0...v1.2.0) (2024-04-17)


### Features

* remove useless jobs ([34f582d](https://gitlab.com/mauriau/autochangelog/commit/34f582d415bec7ae3c7fa5f095033a81c6841a3f))
* remove useless jobs ([20adcdd](https://gitlab.com/mauriau/autochangelog/commit/20adcdd99247d4c0694708015edf45c0c29160e3))

## [1.1.0](https://gitlab.com/mauriau/autochangelog/compare/v1.0.0...v1.1.0) (2024-04-17)


### Features

* update readme ([94624c5](https://gitlab.com/mauriau/autochangelog/commit/94624c523b588443f4d4224c6b4c70c97c5f128a))

## 1.0.0 (2024-04-17)


### Features

* Empty Commit for CI ([0c2d0ee](https://gitlab.com/mauriau/autochangelog/commit/0c2d0ee0f6dc7d59d53e4783b333890306e1ffe4))
* Empty Commit for CI ([8d7cf24](https://gitlab.com/mauriau/autochangelog/commit/8d7cf241ddad89ed6dde63fbc3dd6297d1fa871c))
* init sementic release ([ca14987](https://gitlab.com/mauriau/autochangelog/commit/ca14987cbd32f6497379feabaa52dd7737bb8e9c))
* try trgger changelog after merge on main ([e5530e6](https://gitlab.com/mauriau/autochangelog/commit/e5530e6713c0ff975eb29ee151d0f5b1e1eaac04))


### Bug Fixes

* sementic release ([3a75e0a](https://gitlab.com/mauriau/autochangelog/commit/3a75e0a24e5e38a754213ff5bbc8855b17ab78ca))
* sementic release ([b61c131](https://gitlab.com/mauriau/autochangelog/commit/b61c131addd862a989fb69ca0324d8c9a3188405))
* sementic release is needed before release ([7fd0d88](https://gitlab.com/mauriau/autochangelog/commit/7fd0d8888960e6ee328af801c23d6137bfd00686))
* semntic release is needed before release ([c12c334](https://gitlab.com/mauriau/autochangelog/commit/c12c334d67cc3f4baac5e9721872942b0f0ab84d))
* semntic release is needed before release ([15cf33c](https://gitlab.com/mauriau/autochangelog/commit/15cf33c00e88de739f70143d09fd4216289bf3dc))
* semntic release is needed before release ([137d2bc](https://gitlab.com/mauriau/autochangelog/commit/137d2bc99d88fd999b2a32cf363d8557c44c5b0b))
* try with deploy and release only ([a4b0770](https://gitlab.com/mauriau/autochangelog/commit/a4b0770714f4c6c3849c879f018be9e87a25a845))
* try with deploy and release only ([c31fc9b](https://gitlab.com/mauriau/autochangelog/commit/c31fc9bd7e304ee495388fa59373c6f5ab9b0444))


### Miscellaneous Chores

* try to write changelog ([313a2de](https://gitlab.com/mauriau/autochangelog/commit/313a2dede61d3a79c4b37942051d0d1a462df53f))
* try with tag ([765e69a](https://gitlab.com/mauriau/autochangelog/commit/765e69a01b776ff9bf29697c4a03377991e2ffbb))
* update readme ([4218bb3](https://gitlab.com/mauriau/autochangelog/commit/4218bb30d91e4600337ce8884057cb395b921b70))
* use node 20 ([3ff74d6](https://gitlab.com/mauriau/autochangelog/commit/3ff74d62e32c9e19b75a1168c52a996e4a2ac4e0))
* use node 20 ([2df2b82](https://gitlab.com/mauriau/autochangelog/commit/2df2b827fe1850f13ac1513d81046085cbe33956))
* use yml file instead of yaml ([02d350e](https://gitlab.com/mauriau/autochangelog/commit/02d350e06c38a80a8ff31c00b548313097cbd33e))

3 - third

2 - second

1 - fist
