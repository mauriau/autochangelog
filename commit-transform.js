// commit-transform.js
module.exports = (commit, context) => {
    if (commit.type === 'merge') {
        commit.type = 'Merges';
        if (commit.subject && commit.subject.includes('Migration:')) {
            commit.notes.push({
                title: 'Migrations',
                text: commit.subject.replace('Migration:', '').trim()
            });
        }
    }
    return commit;
};